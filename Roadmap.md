## Roadmap

#### Proof of concept
We are working in a proof of concept to validate if we can configure a Nextcloud docker image to run by default against an OpenLDAP.

At the same time we are starting to learn node.js and express to build an script to run and manage both images.

#### Next steps

- Administration platform
  - Initial user should have a normal account and should create a second password that will be the administration password.
  - Normal account is created in the LDAP and all services should authenticate users against this LDAP.
  - With administration password, user could run or stop services.
  - Sections (all of them with administration password)
    - Manage services with info about how to use them and where you can download mobile apps for this services
    - Manage users
    - Backups
    - Parental control

- Backups
  - Store all persistent data from containers inside a directory and execute an rsync each (day, month...)
  - User will have an interface in the administration pannel to choose the place to store the backup between connected storage devices.  
  - Everything should be ciphered!


- Parental control
  - Investigate if we can do ARP spoofing to configurate this platform as default gateway in the network.
  - Filter by domains, IPs, whitelist, blacklists and even the posibility to create groups like (online bets, adult content...)


- DNS
  - Posibility to use our DNS using BIND to access to our services.


- A way to have our device reachable from the internet.
  - Investigate about how to open ports in a router in a standar way and check if UPNP could help us.


- SSL certificates
- Investigate what we can do with dynamics IPs to access our device from the internet.
- Choose your own DNS
- Logs management
- Integrate software inside a hardware device, adding wifi for the first configuration.
