

## cacharro-libre


### What is it?
We are building an easy way to run and manage your cloud inside your home, just run the app, select the services that you need and use them.

We will run this application inside a box, under a Raspberry Pi or a similar device that you just need to connect to your router in your home, use the wifi created by the device to configure it in a very simple way, and run your own calendar, contacts app, notes, parental control, email server... Everything ready and reacheble from the internet, without configuration.


### Planned services to be added

- Nextcloud
- LDAP
- Backup system
- Parental control
- Email
- Let's encrypt
- Log system
- Open VPN
- ...

You can check our [roadmap](./Roadmap.md).

### License

GNU General Public License (GPL) v3.0

### How to contribute
We are following git flow pattern:

- `master`: production branch (updated on each release)
- `develop`: development branch (unstable branch updated when each feature/bug is added)
- `feature/bugfix`: For each feature/bugfix a new branch should be created based on `develop`, feature will be added here and then you should do a Merge Request to `develop`.
